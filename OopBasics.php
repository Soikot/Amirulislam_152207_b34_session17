<?php
class StudentInfo
{
    public $std_name;
    public $std_id;
    public $std_cgpa;

    public function set_std_id($std_id){
        $this ->std_id=$std_id;
    }

    public function set_std_name($std_name){
        $this ->std_name=$std_name;
    }

    public function get_std_id(){
        return $this->std_id;
    }

    public function get_std_name(){
        return $this->std_name;
    }

    public function set_std_cgpa($std_cgpa){
        $this-> std_cgpa=$std_cgpa;
    }

    public function get_std_cgpa(){
        return $this->std_cgpa;
    }

}
$obj=new StudentInfo();
$obj->set_std_id("seip152207");
$result= $obj->get_std_id();
echo $result;
$obj->set_std_name("Soikot");
$result1= $obj->get_std_name();
echo $result1;
$obj->set_std_cgpa("3.98");
$result2=$obj->get_std_cgpa();
echo $result2;